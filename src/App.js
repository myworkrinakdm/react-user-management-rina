import React from 'react';
import './App.css';
import HomeComponent from './Components/HomeComponent/HomeComponent';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './Reducers';

const store = createStore(reducers, 
  window.devToolsExtension && window.devToolsExtension()
)

class App extends React.Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className='container'>header</div>
        </header>
        <Provider store={store}>
          <HomeComponent></HomeComponent>
        </Provider>
        <footer>
          &copy; Copyright 2020
        </footer>
      </div>
    );
  }
  
}

export default App;
