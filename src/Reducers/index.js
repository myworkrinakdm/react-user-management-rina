import { combineReducers } from 'redux';
import UsersReducer from './reducer_users'
import AccountReducer from './reducer_account';

const rootReducer = combineReducers({
    users:UsersReducer,
    account:AccountReducer
})

export default rootReducer;