import './HomeComponent.css';
import React, { Component } from 'react';
import { connect } from 'react-redux';

class HomeComponent extends Component {
    

    render() {
        return (
            <div className='main-user-list-container container'>
                <h3>User management homepage</h3>

                <div className='user-list-table'>
                    <div className='user-list-heading'>
                        <div className='td'>Sr.No.</div>
                        <div className='td'>User Name</div>
                        <div className='td'>Account Id</div>
                        <div className='td'>App Title</div>
                    </div>

                    {/* {console.log('users', this.props.userStore)} */}
                    {Object.entries(this.props.userStore).map(function(userdata, index) {

                        return (
                            <div className='user-list-body'>
                                <div className='td'>{index + 1}</div>
                                <div className='td'>{userdata[1].name}</div>
                                <div className='td'>{userdata[1].account}</div>
                                <div className='td'>Fargo</div>
                            </div>
                        )
                    }
                    )}
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return { userStore: state.users }
}

export default connect(mapStateToProps)(HomeComponent);